<?php

function fetch_item($user_id)
{
    global $connection;
    $sql = "select * from item group by category,item_id";
    $result = mysqli_query($connection, $sql);
    if (!$result) die("Fetching items failed");
    return $result;
}
function fetch_bank($user_id)
{
    global $connection;
    $sql = "select * from bank_account where p_id = ".$user_id;
    $result = mysqli_query($connection, $sql);
    if (!$result) die("Fetching items failed");
    return $result;
}
function fetch_adress($user_id)
{
    global $connection;
    $sql = "select * from address where p_id = ".$user_id;
    $result = mysqli_query($connection, $sql);
    if (!$result) die("Fetching items failed");
    return $result;
}
function show_item_table($result,$user_id)
{
    echo "<h2>to purchase something click on the item id link shown in table</h2>";
    echo "<table>";
    echo "<th>category</th>";
    echo "<th>item_name</th>";
    echo "<th>item_rating</th>>";
    echo "<th>purchase link</th>";
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo("<tr>");
            echo "<td>".$row['category']."</td>";
            echo "<td>".$row['item_name']."</td>";
            echo "<td>".$row['rating']."</td>";
            $pur_link = "item.php?item_id=".$row['item_id']."&user_id=".$user_id."&item_name=".$row['item_name'];
            echo "<td><a href=".$pur_link.">click to see the available seller</a></td>";
            echo "</tr>";


        }
    }
    echo "</table>";

}
function show_bank_table($result,$user_id)
{
    echo "<h2>previous bank details of the user</h2>";
    echo "<table>";
    echo "<th>bank name</th>";
    echo "<th>card number</th>";
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo("<tr>");
            echo "<td>".$row['bank_name']."</td>";
            echo "<td>".$row['card_no']."</td>";
            echo "</tr>";


        }
    }
    echo "</table>";

}

function show_adress_table($result,$user_id)
{
    echo "<h2>previous adress details of the user</h2>";
    echo "<table>";


    echo "<th>state</th>";
    echo "<th>city</th>";
    echo "<th>pincode</th>";
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo("<tr>");
            echo "<td>".$row['state']."</td>";
            echo "<td>".$row['city']."</td>";
            echo "<td>".$row['pincode']."</td>";
            echo "</tr>";


        }
    }
    echo "</table>";

}
function avail_seller($item_id)
{
    global $connection;
    $sql = "select s.seller_id,sl.rating,p.name,s.item_id,s.price,s.qty from sell as s, seller as sl, person as p where s.item_id =".$item_id." and s.seller_id=p.p_id and s.qty > 0 and sl.seller_id = s.seller_id";
//    echo $sql;
    $result = mysqli_query($connection, $sql);

    if (!$result)
        die("items failed".mysqli_get_server_info($connection));
    else
        return $result;

}

function show_seller_table($result,$user_id)
{

    echo "<p>this is in show table and the user id is".$user_id."</p>";
    while ($row = mysqli_fetch_assoc($result)) {

        echo("<tr>");

        echo "<td>" . $row['name'] . "</td>";
        echo "<td>" . $row['rating'] . "</td>";
        echo "<td>" . $row['price'] . "</td>";


        echo"<form action='' method='post'><td><input type='number' name='qty' value=".$row[qty]."></td><td><input type='submit' name='submit' value ='add to cart' ></td><td><input type='hidden' name='item_id' value=".$row['item_id']."></td><td><input type='hidden' name='seller_id' value=".$row['seller_id']."></td><td><input type='hidden' name='user_id' value=".$user_id."></td></form>";

        echo "</tr>";
    }
}

function cart_item($uid)
{
    global $connection;
//    echo "your uid is " . $uid;
    $sql = "select s.seller_id, c.item_id, i.item_name,c.qty,p.name as seller,s.price from item as i,cart as c, person as p, sell as s where c.user_id=" . $uid . " and i.item_id = c.item_id and p.p_id = c.seller_id and s.item_id = c.item_id and s.seller_id = c.seller_id";

    $result = mysqli_query($connection, $sql);

    if (!$result)
        die("Fetching items failed" . mysqli_get_server_info($connection));
    return $result;
}


function show_cart_item($result,$uid)
{


        while ($row = mysqli_fetch_assoc($result)) {

            echo("<tr>");
            echo "<td>".$row['item_name']."</td>";
            echo "<td>".$row['seller']."</td>";
            echo "<td>".$row['price']."</td>";
            $pur_link = "wallet.php?uid=".$uid."&qty=".$row['qty']."&item_id=".$row['item_id']."&seller_id=".$row['seller_id'];
//            $cart_link = "cart.php?uid_new=".$uid."&item_id_new=".$row['item_id'];
            echo"<form action='' method='post'><td><input type='number' name='qty' value=".$row[qty]."></td><td><input type='submit' name='submit' value ='update cart'></td><td><input type='hidden' name='user_id' value=".$uid."></td><td><input type='hidden' name='item_id' value=".$row['item_id']."></td><td><input type='hidden' name='seller_id' value=".$row['seller_id']."></td></form>";
//            echo "<td><a href=".$cart_link.">click to change the cart qty</a></td>";
            echo "<td><a href=".$pur_link.">click to purchase now</a></td>";
            echo "</tr>";



    }
}

function fetch_sell_information($item_id,$user_id)
{
    global $connection;
    $sql = "select p.name,s.price,s.seller_id".
    "from sell as s, person as p".
    "where s.item_id=".$item_id.
    "and p.p_id=s.seller_id".
    "and s.qty>0";
    $result = mysqli_query($connection, $sql);
    if (!$result) die("Fetching items failed");

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

            echo("<tr>");
            foreach ($row as $key => $value) {

                echo "<td>" . $value . "</td>";
            }

            /*write code for quantity also*/
            echo "<td><input type=number name='qty'></td>";
            $link = "item.php?item_id=".$item_id.
                "&user_id=".$user_id.
                "&seller_id=".$row['seller_id'].
                "&price=".$row['price'];
            echo "<td><a href=".$link.">add to cart</a></td>";
            echo "</tr>";

        }
    }

}

function insert_cart($user_id,$item_id,$seller_id,$qty)
{
    global $connection;
    $query = "select * from cart where user_id=".$user_id." and item_id=".$item_id." and seller_id=".$seller_id;
    $qresult = mysqli_query($connection,$query);
//    echo "inside insert";
//    echo $query;

    if (!$qresult)
        die("query for insertion failed");
    if (mysqli_num_rows($qresult) > 0)
    {

        $val = update_cart($user_id,$item_id,$seller_id,$qty);
        return $val;
    }
//    echo "came for inserting";

    $sql = "insert into cart values (".$user_id.",".$item_id.",".$qty.",".$seller_id.")";
//    echo $sql;
    $result =  mysqli_query($connection, $sql);
    if(!$result)
    {
        echo "insertion failed maybe duplicate entry";
        return 0;
    }
    else
    {
        echo "inserted successfully";
        return 1;

    }
}

function update_cart($user_id,$item_id,$seller_id,$qty)
{
    global $connection;
    $sql = "update cart set qty =".$qty." where user_id = ".$user_id." and item_id = ".$item_id." and seller_id = ".$seller_id;
    echo $sql;
    $result =  mysqli_query($connection, $sql);
    if(!$result)
    {
        echo "updation failed";
        return 0;

    }
    else
    {
        echo "updated successfully";
        return 1;

    }

}

function wallet_checker($uid,$qty,$item_id,$seller_id,$t)
{
    global $connection;
    $sql = "CALL delete_cart($uid, $item_id, $seller_id,$qty,$t,@output_n)";
    if(mysqli_query($connection, $sql)){
        echo "Procedure test_proc records added/updated successfully.";
    } else{
        echo "ERROR: Couldn't execute $sql. " . mysqli_error($connection);
    }
    $query = ("select @output_n");
    $result = mysqli_query($connection,$query);
    if (!$result)
        die("query failed");
    $row = mysqli_fetch_assoc($result);
    if ($row['@output_n'])
        echo "transaction successful";
    else
        echo "insufficient balance";


}

function show_wallet($user_id)
{
    global $connection;
    $query = "select * from wallet where customer_id=".$user_id;
//    echo $query;
    $result = mysqli_query($connection,$query);
    if (!$result)
        die("query for insertion failed");
    if (mysqli_num_rows($result) > 0)
    {
        $row = mysqli_fetch_assoc($result);
        echo "<p><br>your balance is ".$row['balance']."</p>";
    }
    else
        echo "query return empty result";

}
